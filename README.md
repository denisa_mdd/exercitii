P1. Write a program which takes an integer as input and prints the sum of its
digits.

e.g. Input: 163
Output: 10


P2. Write a program which takes 2 clock times and establish an order relationship
between these. Print the relationship as in the following example.
e.g. Input: 19:48
19:35


Output: 19:48 > 19:35
Input: 13:25
13:25
Output: 13:25 = 13:25


P3. Given 2 arrays of distinct names, a and b , print the names that are found only
in a and not in b .
e.g. a: [Alice, Bob, Jane, Mary]
b: [John, Andrew, Bob, Jane]


Output: Alice
	Mary


P4. Write a function which will take the name of the file containing email
addresses as parameter, will read from that file all the emails and will print the email
addresses sorted alphabetically after the first letter.


e.g. input.txt: john_smith@gmail.com
andrew@yahoo.com
danle@gmail.com


Output: andrew@yahoo.com
danle@gmail.com
john_smith@gmail.com


P5. Using HTML and CSS, create a web page containing a table:
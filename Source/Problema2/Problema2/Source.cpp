#include<iostream>
#include<string.h>
#include<stdlib.h>
using namespace std;
int main() {
	char t1[7];
	char t2[7];
	cout << "Input:  ";
	cin >> t1;
	t1[6] = '\0';
	cout << "\t";
	cin >> t2;
	t2[6] = '\0';
	if (t1[2] != ':' || t2[2] != ':') {
		cout << "Wrong input!";
		cin.ignore(2);
		exit(EXIT_FAILURE);
	}
	int aux1, aux2;
	aux1 = (t1[0]-'0')*10+(t1[1]-'0');
	aux2 = (t2[0] - '0') * 10 + (t2[1] - '0');
	if (aux1 > 24 || aux2 > 24) {
		cout << "Wrong input!";
		cin.ignore(2);
		exit(EXIT_FAILURE);
	}
	int aux3, aux4;
	aux3 = (t1[3] - '0') * 10 + (t1[4] - '0');
	aux4 = (t2[3] - '0') * 10 + (t2[4] - '0');
		if (aux1 > aux2) {
			cout << endl << "Output:   " << t1 << " > " << t2 << endl;
		}
		if(aux1<aux2){
			cout << endl << "Output:   " << t1 << " < " << t2 << endl;
		}
		if (aux1 == aux2) {
			if (aux3 == aux4) {
				cout << endl << "Output:   " << t1 << " = " << t2 << endl;
			}
			else if (aux3 < aux4) {
				cout << endl << "Output:   " << t1 << " < " << t2 << endl;
			}
			else {
				cout << endl <<"Output:   "<<t1 << " > " << t2 << endl;
			}
		}
	system("PAUSE");
	return 0;
}